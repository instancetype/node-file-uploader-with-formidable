/**
 * Created by instancetype on 6/7/14.
 */
var http = require('http'),
    formidable = require('formidable');

var server = http.createServer(function(req, res) {
   switch (req.method) {

       case 'GET':
           show(req, res);
           break;

       case 'POST':
           upload(req, res);
           break;
   }
}).listen(3000);

function show(req, res) {
    var html = '';
    html += '<form method="post" action="/" enctype="multipart/form-data">'
         + '<p><input type="text" name="name" placeholder="filename"></p>'
         + '<p><input type="file" name="file"></p>'
         + '<p><input type="submit" value="Upload"></p>'
         + '</form>';

    res.setHeader('Content-Type', 'text/html');
    res.setHeader('Content-Length', Buffer.byteLength(html));
    res.end(html);
}

function upload(req, res) {
    if (!isFormData(req)) {
        res.statusCode = 400;
        res.end('Bad Request: Expecting multipart/form-data');
        return;
    }

    var form = new formidable.IncomingForm();

    form.on('progress', function(bytesReceived, bytesExpected) {
        var percent = Math.floor(bytesReceived / bytesExpected * 100);
        console.log('Progress: ' + percent + '%');
    });

    form.parse(req, function(err, fields, files) {
        console.log(fields);
        console.log(files);
        res.end('Upload complete!');
    });
}

function isFormData(req) {
    var type = req.headers['content-type' || ''];
    return 0 === type.indexOf('multipart/form-data');
}